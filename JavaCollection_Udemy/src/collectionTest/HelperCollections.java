package collectionTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HelperCollections {
    public static void main(String[] args) {
        List<Integer> list  = Arrays.asList(1,55,3,8,21, 25, 73,90);
        System.out.println(list); //[1, 55, 3, 8, 21, 25, 73, 90]

        // sort using Collections
        Collections.sort(list);
        System.out.println(list); //[1, 3, 8, 21, 25, 55, 73, 90]
    }
}
