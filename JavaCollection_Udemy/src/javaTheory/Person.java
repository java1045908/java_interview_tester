package javaTheory;

public class Person {
    private String firstName;
    public String lastName;
    public int age;

    public String getFirstName() {
        return  firstName;
    }

    public void setFirstName(String firstName) {
        if(firstName ==null || firstName.isEmpty()) {
            throw new IllegalArgumentException("Invalid firstName: " + firstName);
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age <0) {
            throw  new IllegalArgumentException("Invalid age: " + age);
        }
        this.age = age;
    }
}
