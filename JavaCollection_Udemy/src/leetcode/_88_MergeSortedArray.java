package leetcode;

//https://leetcode.com/problems/merge-sorted-array/description/
public class _88_MergeSortedArray {
    public static void merge(int[] n1, int m, int[] n2, int n) {
        for (int ai : n2) {
            chenPhanTuVaoMang(ai, n1, m);
            m++;
        }
    }

    public static void main(String[] args) {
        int[] n1 = {2, 3, 4, 5, 0, 0, 0};
        int[] n2 = {0, 3, 6};

        // n1 co 4 phan tu (m) chua ra 3 so cho n2, n2 co 3 phan tu
        merge(n1, 4, n2, 3);
        System.out.println("DONE");
    }

    public static void chenPhanTuVaoMang(int x, int[] a, int m) {
        boolean timDuocK = false;
        for (int k = 0; k < m; k++) {
            // Neu phan tu dau tien > x, thi ta se dich tu vi tri
            if (a[k] > x) {
                timDuocK = true;
                for (int i = m - 1; i >= k; k++) {
                    a[i + 1] = a[i];
                }
                a[k] = x;
                break;
            }
        }
        // Neu ko tim dc, chen vao cuoi
        if (timDuocK == false) {
            a[m] = x;
        }
    }


}
