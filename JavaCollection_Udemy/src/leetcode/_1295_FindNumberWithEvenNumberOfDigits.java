package leetcode;

public class _1295_FindNumberWithEvenNumberOfDigits {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4};
        int[] nums1 = {1, 22, 3, 4444};
        int[] nums2 = {1, 22, 33, 4444};
        int[] n2 = new int[4];
        n2[0] = 4;
        n2[1] = 3;
        n2[2] = 2;
        n2[3] = 1;

        System.out.println(findNumbers(nums)); //0
        System.out.println(findNumbers(nums1)); //2 ( 22 co 2 chu so , 44 co 4 chu so)
        System.out.println(findNumbers(nums2)); //3 ( 22 co 2 chu so chan , 33 co 2 chu so chan, 44 co 4 chu so chan)

    }

    public static int findNumbers(int[] nums) {
        int bienDem = 0;

        for (int a : nums) {
            // Kiem tra so chu so cua a
            // Neu nhu so chu so la chan ==> tang bien dem len 1
            int soLuongChuSo = tinhSoChuSo(a);
            if (soLuongChuSo % 2 == 0) {
                bienDem++;
            }
        }
        return bienDem;
    }

    private static int tinhSoChuSo(int a) {
        int bienDem = 0;
        int kq = a;
        while (kq != 0) {
            kq = a / 10;
            a = kq;
            bienDem++;
        }
        return bienDem;
    }
}
