package leetcode;

//https://leetcode.com/problems/remove-element/description/
public class _27_RemoveElement {
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4};
        System.out.println(removeElement(a, 2));
        System.out.println("DONE");
        System.out.println(removeElement2(a, 2));
        System.out.println("DONE");
    }

    public static int removeElement(int[] a, int val) {
        int n = a.length;

        for (int i = 0; i < n; ) {
            if (a[i] == val) {
                //xoa phan tu a[i] ==> don tat ca phan tu ben phai tien them 1 don vi
                for (int j = i; j <= n - 2; j++) {
                    a[j] = a[j + 1];
                }
                n--;
            } else { //Khong phai xoa
                i++;
            }
        }

        return n;
    }

    public static int removeElement2(int[] a, int x) {
        int k = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != x) {
                a[k] = a[i];
                k++;
            } else {
                // Khong lam gi
            }
        }
        return k; // tra ve so phan tu, vo tinh   laf o phan tu
    }
}
