package leetcode;

public class java_709 {
    public static void main(String[] args) {
        System.out.println(toLowerCase("S")); //s
        System.out.println(toLowerCase("SSSSA")); //ssssa

    }

    public static String toLowerCase(String s) {
        //return s.toLowerCase();

        char[] c = s.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] >= 65 && c[i] <= 99) { //uppercase
                c[i] = (char) (c[i] + 32);
            }
        }
        return String.valueOf(c);
    }
}
