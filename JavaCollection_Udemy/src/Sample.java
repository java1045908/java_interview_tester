import java.util.ArrayList;
import java.util.List;

public class Sample {
    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>();
        arr.add(1);
        arr.remove(12); //o(n) | n remove 0,
        arr.clear();
        arr.contains(1); // O(n)
    }
}
