package theoretical.twentynine;

public abstract class Vehicle  {
    public String name;

    public abstract  void drive();
    public abstract  void stop();
}
