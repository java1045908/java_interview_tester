package theoretical.twentynine;

public class Car extends Vehicle{
    @Override
    public void drive() {
        System.out.println("Car is driving");
    }

    @Override
    public void stop() {
        System.out.println("Car is stopped");
    }
}
