package theoretical.twentysix;

public class Person extends Object {
    public String firstName;
    public String lastName;
    public int age;

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }

    public void run(int miles) {
        System.out.println("Person is running " + miles + " miles");
    }
}
