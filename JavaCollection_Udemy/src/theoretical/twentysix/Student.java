package theoretical.twentysix;

import java.util.List;

public class Student extends Person{
    public  long studentId;
    public List<String> courses;

    @Override
    public void run(int miles) {
        System.out.println("Student is running " + miles + " miles");
    }

}
