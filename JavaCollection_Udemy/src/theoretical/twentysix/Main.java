package theoretical.twentysix;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        student.firstName = "John";
        student.lastName = "Doe";
        student.age = 27;
        student.studentId = 123;

        student.run(5); //Student is running 5 miles

    }


}
