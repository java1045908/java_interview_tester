package collectionClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestCollection {
    public static void main(String[] args) {
        //  Adding elements to the Collections
        List<String> items = new ArrayList<>();
        items.add("Shoes");
        items.add("Toys");

        Collections.addAll(items,"Fruits", "Bat", "Ball");
        for (int i = 0; i <items.size(); i++) {
            System.out.println(items.get(i) +"\t\t");
        }
        System.out.println();

        // Sorting a collection
        Collections.sort(items);
        for (int i = 0; i <items.size(); i++) {
            System.out.println(items.get(i) +"\t\t");
        }
        System.out.println();

        Collections.sort(items,Collections.reverseOrder());
        for (int i = 0; i <items.size(); i++) {
            System.out.println(items.get(i) +"\t\t");
        }
        System.out.println();

        // Searching a Collection
        Collections.sort(items);

        // Binary Search on the List
        System.out.println("The index of Toys is " + Collections.binarySearch(items,"Toys"));

        List<String> source_list = new ArrayList<>();
        source_list.add("Bat");
        source_list.add(("Frog"));
        source_list.add("Lion");

        // Copy the elements from source to destination
        Collections.copy(items, source_list);

        //Printing the modified list
        System.out.println("The Destination List After copying is: ");
        for (int i = 0; i <items.size() ; i++) {
            System.out.println(items.get(i));
        }
    }


}
