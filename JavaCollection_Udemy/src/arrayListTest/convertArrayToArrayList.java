package arrayListTest;

import java.util.ArrayList;
import java.util.Arrays;

public class convertArrayToArrayList {
    public static void main(String[] args) {
        // Convert Array to ArrayList
        String[] array = {"Dog", "Cat", "Chicken"};
        ArrayList<String> animals = new ArrayList<String>(Arrays.asList(array));
        System.out.println("Animal: " + animals); //Animal: [Dog, Cat, Chicken]
     }
}
