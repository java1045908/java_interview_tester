package arrayListTest;

/*
 For Ex:
The Input is : "RahulShettyAcademy"
Output should be : "ymedacAyttehSluhaR"
 */
public class ReverseString {
    public static void main(String[] args) {
        String inputString = "RahulShettyAcademy";
        StringBuilder outputString = new StringBuilder();
        for (int i = inputString.length() -1; i>0; i--) {
            outputString.append(inputString.charAt(i));
        }
        System.out.println(outputString);
    }
}