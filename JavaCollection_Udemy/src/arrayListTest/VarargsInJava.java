package arrayListTest;

public class VarargsInJava {
    public static void main(String[] args) {
        int[] arrNum = {8, 6, 7, 100, 200, 4, 33, 12};
        int res = max(arrNum);
        System.out.println(res); //200

        res = max(4,5);
        System.out.println(res); //5

        res = max(9,7,3); //9
        System.out.println(res);
    }

    // Return the biggest number in array
    public static int max(int... numbers) {
        int biggest = numbers[0];
        for (int i = 0; i < numbers.length; i++) {
            if (biggest < numbers[i]) {
                biggest = numbers[i];
            }
        }
        return biggest;
    }
}

