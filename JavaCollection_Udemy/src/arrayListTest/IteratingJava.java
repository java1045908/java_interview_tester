package arrayListTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class IteratingJava {

    public static void main(String[] args) {
        // Using for
        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6);
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i) +"");
        }
        System.out.println("------USING WHILE ------------");
        //Using While
        int number = 0;
        while (numbers.size() >number) {
            System.out.println(numbers.get(number) +"");
            number++;
        }
        System.out.println("------USING FOR EACH ------------");
        // For each loop
        for(int i: numbers) {
            System.out.println(i + " ");
        }
        System.out.println("------USING LAMBDA EXPRESSION ------------");
        // Using lambda Expression
        numbers.forEach(num -> System.out.println(num));

        System.out.println("------SORTING ------------");
        //Collections.sort()
        ArrayList<String> animals = new ArrayList<>();
        animals.add("Dog");
        animals.add("Cat");
        animals.add("Chicken");
        animals.add("Pig");
        Collections.sort(animals);
        System.out.println(animals); //[Cat, Chicken, Dog, Pig]

    }
}
