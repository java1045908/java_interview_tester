package arrayListTest;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExample {
    public static void main(String[] args) {
       // Set<String> fruitBasket = new HashSet<>();
        //Set<String> fruitBasket = new LinkedHashSet<>();
        Set<String> fruitBasket = new TreeSet<>();
        fruitBasket.add("apple");
        fruitBasket.add("pear");
        fruitBasket.add("kiwi");
        fruitBasket.add("banna");
        fruitBasket.add("apple");
        System.out.println(fruitBasket); //[apple, banna, kiwi, pear]
    }
}
