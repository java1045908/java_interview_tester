package arrayListTest;

import java.util.Arrays;

public class HeplerArrays {
    public static void main(String[] args) {
        int[] myArr = {1,55,3,8,21,25,73,90};

        System.out.println(myArr);// [I@36baf30c
        //print using Arrays
        System.out.println(Arrays.toString(myArr)); //[1, 55, 3, 8, 21, 25, 73, 90]

        // sort using Arrays
        Arrays.sort(myArr);
        System.out.println(Arrays.toString(myArr)); //[1, 3, 8, 21, 25, 55, 73, 90]

        int[] myArr2 = {0,4,5,3};
        // compare two arrays using Arrays
        System.out.println(Arrays.equals(myArr, myArr2)); //false

    }
}
