package arrayListTest;

public class JavaConstructor {
    public static void main(String[] args) {
        Book book = new Book("Java 101", 200);
        Book book1 = new Book("Js");
    }
}

class Book {
    public  String title;
    public  int pages;

    public  Book(String title, int pages) {
        System.out.println("In the constructor");
        this.title = title;
        this.pages = pages;
    }
    // Constructor overloading
    public Book(String title ) {
        System.out.println("In the constructor with one arguments");
        this.title = title;
    }
}
