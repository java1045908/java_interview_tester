package coding;

import java.util.Arrays;

public class SecondLargestNumber {
    public static void main(String[] args) {
        int a[] = {1, 2, 5, 6, 3, 2};
        int b[] = {44, 66, 99, 77, 33, 22, 55};
        System.out.println("Second Largest: " + getSecondLargest(a, 6)); //5
        System.out.println("Second Largest: " + getSecondLargest(b, 7)); //77
    }

    public static int getSecondLargest(int[] arr, int total) {
        Arrays.sort(arr);
        return arr[total - 2];
    }
}
