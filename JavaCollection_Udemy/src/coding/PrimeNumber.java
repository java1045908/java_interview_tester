package coding;

public class PrimeNumber {
    public static void main(String[] args) {
        System.out.println(isPrime(29)); //true
        System.out.println(isPrime(35)); //false
        System.out.println(isPrime(20)); //false
        System.out.println(isPrime(27)); //false
        System.out.println(isPrime(7)); //true

    }

    /**
     * A prime number is a number that can only be divided by itself and 1 without remainder
     * 7 -> true
     * 29 -> true
     * 35 -> false (because 35 can divide by 5 or 15)
     */

    public static boolean isPrime(int nunm) {
        // num = 7

        //     2    ; 2<=3
        for (int i = 2; i <= nunm / 2; i++) {
            // 7 % 2 = 1
            // 1 ==0 (false) ==> true
            if (nunm % i == 0) {
                return false;
            }
        }
        return true;
    }
}