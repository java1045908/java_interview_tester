package coding;

public class MinNumber {
    public static void main(String[] args) {
        System.out.println(min(new int[]{4, 781, 8, 99, 103})); //4
        System.out.println(min(new int[]{1, 2, 3, 4, 5})); //1
        System.out.println(min(new int[]{3, 4})); //3
        System.out.println(min(new int[]{100})); //100
    }

    public static int min(int[] arr) {
        int min = arr[0]; //assume first element is biggest

        // check assumption
        for (int num : arr) {
            // if max wasn't the biggest number, then update it
            if (min > num) {
                min = num;
            }
        }
        return min;
    }
}
