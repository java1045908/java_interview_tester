package coding;

import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicatesFromStr {
    public static void main(String[] args) {
        System.out.println(removeDup("hello")); //helo
        System.out.println(removeDup("applep")); //aple
        System.out.println(removeDup("aaaaaaaa")); //a
        System.out.println(removeDupWithSet("hello")); //helo
        System.out.println(removeDupWithSet("applep")); //aple
        System.out.println(removeDupWithSet("aaaaaaaa")); //a
    }

    /**
     * removeDup("hello") -> helo
     * removeDup("applep") -> aple
     * removeDup("aaaaaaaa") -> a
     * removeDup("hello") -> helo
     */
    public static String removeDup(String str) {
        // "hello" -> helo
        StringBuilder strNoDup = new StringBuilder();
        //  [ h, e, l , l ,o]
        for (char ch : str.toCharArray()) {
            // doesn't contains 'h'
            if (!strNoDup.toString().contains(String.valueOf(ch))) {
                //append
                strNoDup.append(ch);
            }

        }
        return strNoDup.toString();
    }

    // Or do with set
    public static String removeDupWithSet(String str) {
        // load to set to remove duplicate
        Set<Character> set = new LinkedHashSet<>();
        for (char ch : str.toCharArray()) {
            set.add(ch);
        }
        // convert back to string
        StringBuilder strNoDup = new StringBuilder();
        for (char ch : set) {
            strNoDup.append(ch);
        }
        return strNoDup.toString();
    }
}
