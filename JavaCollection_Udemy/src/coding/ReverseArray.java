package coding;

import java.util.Arrays;

/**
 * Reverse the array and print the largest number in array
 */
public class ReverseArray {
    public static void main(String[] args) {
        int[] inputArr = new int[]{1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(inputArr)); //[1, 2, 3, 4, 5]
        revArr(inputArr);
        System.out.println(Arrays.toString(inputArr)); //[5, 4, 3, 2, 1]
        System.out.println(largestNumber(inputArr));
    }

    public static void revArr(int[] arr) {
        //    s     e
        //[1, 2, 3, 4, 5]
        //[5, 2, 3, 4, 1]
        //[5, 4, 3, 2, 1]
        //[5, 4, 3, 2, 1]

        //0
        int start = 0;
        //4
        int end = arr.length - 1;
        // 0 <4 -> true
        while (start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    public static int largestNumber(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
            return max;
        }
        return max;
    }
}
