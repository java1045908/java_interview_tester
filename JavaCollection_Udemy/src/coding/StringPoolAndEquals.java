package coding;

public class StringPoolAndEquals {
    public static void main(String[] args) {
        String str = "apple"; //string pool
        String str1 = "apple";
        String str2 = new String("apple"); //heap memory

        // == checks if two reference pointing to the same object or not
        System.out.println(str == str1); //true
        System.out.println(str == str2); //false
        System.out.println(str.equals(str2)); //true (check content of string)
    }
}
