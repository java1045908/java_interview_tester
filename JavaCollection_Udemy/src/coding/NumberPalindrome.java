package coding;

public class NumberPalindrome {
    public static void main(String[] args) {
        System.out.println(isPal(545)); //true
        System.out.println(isPal(1001)); //true
        System.out.println(isPal(13)); //false
        System.out.println(isPal(33)); //true
    }

    /**
     * A palindrome is a word, pharse, number, or sequence of words that reads the same backward as forward
     * isPal(545) -> true
     * isPal(1001) -> true
     * isPal(13) -> false
     * isPal(33) -> true
     */

    public static boolean isPal(int num) {
        // 1. To get the most right number, we can do 'right' = num % 10;
        // 2. To remove right number, we can do 'num = num/10'
        // 3. To concatenate to the number, we can do 'num = (num * 10) + singleDigitNumber'

        int copy = num;
        int rev = 0;
        int rightMost;


        // 123
        while (num > 0) {
            // 3 = 123 % 10
            rightMost = num % 10; // get right most
            // 3 = 0 * 10 + 3
            rev = (rev * 10) + rightMost; //concatenate to rev
            // 123 / 10 = 12
            num = num / 10; //remove right most from num
        }

        return copy == rev;
    }
}
