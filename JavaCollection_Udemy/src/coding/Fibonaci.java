package coding;

public class Fibonaci {
    public static void main(String[] args) {
        fib(3); // 0 1 1
        fib(5); // 0 1 1 2 3
        fib(6); // 0 1 1 2 3 5
        fib(10); // 0 1 1 2 3 5 8 13 21 34

    }

    /**
     * The fibonacci is a series of numbers where next number is the sum of the previous two numbers
     * The first two number of the Fibonacci is 0 followed by 1
     * Print n Fibonacci numbers in one line separated by space
     */
    public static void fib(int n) {
        int numOne = 0;
        int numTwo = 1;
        System.out.print(numOne + " ");
        System.out.print(numTwo + " ");

        for (int i = 0; i < n - 2; i++) {
            int sum = numOne + numTwo;
            System.out.print(sum + " ");

            numOne = numTwo;
            numTwo = sum;
        }
        System.out.println();
    }
}
