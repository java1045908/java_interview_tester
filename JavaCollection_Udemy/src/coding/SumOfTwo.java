package coding;


import java.util.Arrays;

public class SumOfTwo {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(sumOfTwo(new int[]{1, 2, 3, 5}, 4))); //[1, 3]
        System.out.println(Arrays.toString(sumOfTwo(new int[]{7, 7, 4, 3, 8}, 7))); //[4, 3]
        System.out.println(Arrays.toString(sumOfTwo(new int[]{13, 43, 2, 71}, 84))); //[13, 71]
        System.out.println(Arrays.toString(sumOfTwo(new int[]{1, 2, 3, 4}, 100))); //[0, 0]
    }

    /**
     * sum([1,2,3,5],4) -> [1,3]
     * sum([1,2,3,4],100) -> [0,0]
     */

    public static int[] sumOfTwo(int[] arr, int target) {
        int[] res = new int[2];

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] + arr[j] == target) {
                    res[0] = arr[i];
                    res[1] = arr[j];
                    return res;
                }
            }
        }

        return res;
    }
}
