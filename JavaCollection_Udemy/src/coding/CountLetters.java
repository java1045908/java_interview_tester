package coding;

import java.util.LinkedHashMap;
import java.util.Map;

public class CountLetters {
    public static void main(String[] args) {
        System.out.println(countLetters("hello")); //{h=1, e=1, l=2, o=1}
        System.out.println(countLetters("auucchhhh")); //{a=1, u=2, c=2, h=4}
        System.out.println(countLetters("aaaaaaa")); //{a=7}
        System.out.println(countLetters("abc")); //{a=1, b=1, c=1}
        System.out.println(countLetters("abca")); //{a=2, b=1, c=1}
    }

    /**
     * find out how many of each letter exist in the string
     * countLetters("hello") -> {h =1, e= 1, l=2, i =1}
     */

    public static Map<Character, Integer> countLetters(String str) {
        Map<Character, Integer> letters = new LinkedHashMap<>();

        for (char ch : str.toCharArray()) {
            //  [h,e,l,l,o]
            if (letters.containsKey(ch)) {
                int count = letters.get(ch);
                letters.put(ch, count + 1);
            } else { //Neu ko chua thi no chi xuat hien 1 lan
                letters.put(ch, 1);
            }
        }
        return letters;

    }
}
