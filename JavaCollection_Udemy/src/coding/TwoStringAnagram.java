package coding;

import java.util.Arrays;

public class TwoStringAnagram {
    public static void main(String[] args) {
        System.out.println(isAnagram("listen", "silent"));//true
        System.out.println(isAnagram("triangle", "integral"));//true
        System.out.println(isAnagram("abc", "bac"));//true
        System.out.println(isAnagram("abc", "ccb"));//false
        System.out.println(isAnagram("aaa", "aaab"));//false
    }

    /**
     * An  anagram is when all the letters is one string exist in another
     * but the order of letters does not matter
     */
    public static boolean isAnagram(String str, String str1) {
        // convert both strings to char[]
        // [l,i,s,t,e,n]
        char[] arrStr = str.toCharArray();

        //[s,i,l,e,n,t]
        char[] arrStr1 = str1.toCharArray();

        // sort both char[] arrays
        Arrays.sort(arrStr);
        Arrays.sort(arrStr1);
        //  System.out.println(Arrays.toString(arrStr));// [e, i, l, n, s, t]
        // System.out.println(Arrays.toString(arrStr1)); // [e, i, l, n, s, t]
        // compare sorted char[]
        return Arrays.equals(arrStr1, arrStr);
    }
}
