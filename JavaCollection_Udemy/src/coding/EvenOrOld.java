package coding;

public class EvenOrOld {
    public static void main(String[] args) {
        evenOrOldd(5); //Odd
        evenOrOldd(2); //Even
        evenOrOldd(100); //Even
        evenOrOldd(101); //Odd
    }

    public static void evenOrOldd(int num) {
        if (num % 2 == 0) {
            System.out.println("Even");
        } else {
            System.out.println("Odd");
        }
    }
}
