package coding;

import java.util.Stack;

public class BalancedStr {
    public static void main(String[] args) {
        System.out.println(isBalanced("[{()}]")); // true
        System.out.println(isBalanced("[({(})]")); // false
        System.out.println(isBalanced("{[}")); // false
        System.out.println(isBalanced("({}{}([{}]))")); // true
        System.out.println(isBalanced("({")); // false
    }

    public static boolean isBalanced(String str) {
        Stack st = new Stack<>();

        for (char ch : str.toCharArray()) {
            if (ch == '{' || ch == '[' || ch == '(') {
                st.push(ch);
            } else {
                if (st.isEmpty()) {
                    return false;
                }
                char lOpen = (char) st.pop();
                if (ch == '}' && lOpen != '{') {
                    return false;
                } else if (ch == ']' && lOpen != '[') {
                    return false;
                } else if (ch == ')' && lOpen != '(') {
                    return false;
                }
            }
        }

        return st.isEmpty();
    }
}
