package coding;

public class reverseWords {
    public static void main(String[] args) {
        System.out.println(revWords("apple banana kiwi")); // kiwi banana apple
        System.out.println(revWords("I am John Doe")); // kiwi banana apple
        System.out.println(revWords("orange")); // kiwi banana apple
    }


    public static String revWords(String str) {
        // "apple banana kiwi"
        // [apple banana kiwi]
        //Create result variable
        StringBuilder result = new StringBuilder();
        // Split str by space " " to get each word as array
        String[] words = str.split(" ");
        // Loop over the array from back
        for (int i = words.length - 1; i >= 0; i--) {
            result.append(words[i]).append(" ");
        }

        return result.toString().trim();
    }
}
