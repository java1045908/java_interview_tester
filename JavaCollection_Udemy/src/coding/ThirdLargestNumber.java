package coding;

public class ThirdLargestNumber {
    public static void main(String[] args) {
        int arr1[] = {12, 13, 1, 10, 34, 16, 16, 16};
        int n1 = arr1.length;

        int arr2[] = {1, 2};
        int n2 = arr2.length;

        thirdLargest(arr1, n1); //13
        System.out.println("\n----------");
        thirdLargest(arr2, n2); //Invalid input
    }

    public static void thirdLargest(int[] arr, int arr_size) {
        // Should be at least 3 number
        if (arr_size < 3) {
            System.out.println("Invalid input");
            return;
        }
        // Find first largest number
        int first = arr[0];
        for (int i = 0; i < arr_size; i++) {
            if (arr[i] > first) {
                first = arr[i];
            }
        }

        //Find second largest number
        int second = Integer.MIN_VALUE;
        for (int i = 0; i < arr_size; i++) {
            if (arr[i] > second && arr[i] < first) {
                second = arr[i];
            }
        }
        // Find third largest number
        int third = Integer.MIN_VALUE;
        for (int i = 0; i < arr_size; i++) {
            if (arr[i] > third && arr[i] < second) {
                third = arr[i];
            }
        }
        System.out.printf("The third Largest " + "element is %d ", third);
    }

}
