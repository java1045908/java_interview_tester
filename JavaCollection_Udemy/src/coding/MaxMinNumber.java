package coding;

import java.util.Arrays;

public class MaxMinNumber {
    public static void main(String[] args) {
        System.out.println(secMax(new int[]{4, 781, 8, 99, 103})); // 103
        System.out.println(secMax(new int[]{1, 2, 3, 4, 5})); //4
        System.out.println(secMax(new int[]{3, 4})); //3

        System.out.println("-------------");
        System.out.println(secMin(new int[]{4, 781, 8, 99, 103})); // 8
        System.out.println(secMin(new int[]{1, 2, 3, 4, 5})); // 2
        System.out.println(secMin(new int[]{3, 4})); //4

        System.out.println("-------------");
        System.out.println(nMax(new int[]{4, 3, 2, 8, 9, 5}, 1)); // 9
        System.out.println(nMax(new int[]{4, 3, 2, 8, 9, 5}, 2)); // 8
        System.out.println(nMax(new int[]{4, 3, 2, 8, 9, 5}, 3)); // 5
        System.out.println(nMax(new int[]{4, 3, 2, 8, 9, 5}, 100)); // 0

    }

    /**
     * secMax({4, 781, 8, 99, 103}) -> 103
     * secMax ({1, 2, 3 , 4, 5}) -> 4
     * secMax ({3,4}) -> 3
     */

    public static int secMax(int[] arr) {
        // 1. Get max/min and save index
        int maxIndex = getMaxIndex(arr, -1);

        // 2. Get max/min by avoiding first max
        return arr[getMaxIndex(arr, maxIndex)];

    }

    private static int getMaxIndex(int[] arr, int avoidIndex) {
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < arr.length; i++) {
            if (i != avoidIndex && max < arr[i]) {
                max = arr[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    /**
     * secMin({4, 781, 8, 99, 103}) -> 103
     * secMin ({1, 2, 3 , 4, 5}) -> 4
     * secMin ({3,4}) -> 3
     */

    public static int secMin(int[] numArr) {
        Arrays.sort(numArr);
        if (numArr.length > 1) {
            return numArr[1];
        }
        return numArr[0];
    }

    /**
     * Return n biggest value from array
     * nMax([4,3,2,8,9,5], 1); -> 9
     * nMax([4,3,2,8,9,5], 2); -> 8
     * nMax([4,3,2,8,9,5], 3); -> 5
     * nMax([4,3,2,8,9,5], 100); -> 0
     */
    public static int nMax(int[] arr, int n) {
        if (arr.length < n) {
            return 0;
        }
        Arrays.sort(arr);
        return arr[arr.length - n];
    }
}
