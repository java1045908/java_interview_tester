package coding;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] arrNum = {6, 5, 2, 1, 9, 18, 0};
        System.out.println(Arrays.toString(arrNum)); // [6, 5, 2, 1, 9, 10, 8]
        bSort(arrNum);
        System.out.println(Arrays.toString(arrNum));// [0, 1, 2 ,5 ,6 ,9 ,10]
    }

    /**
     * Bubble Sort:
     * Pseudocode:
     * - Set swap counter to a-1
     * -Repeat until the swap counter is 0
     * - Reset swap counter to 0
     * - Look at each adjacent pair
     * - If two adjacent elements are not in order, swap then and add one to the swap counter
     */
    public static void bSort(int[] arr) {
        int swapCounter = -1;
        int unsortedArrayLength = arr.length;

        while (swapCounter != 0) {
            swapCounter = 0;

            for (int i = 0; i < unsortedArrayLength - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    int temp = arr[i + 1];
                    arr[i + 1] = arr[i];
                    arr[i] = temp;
                    swapCounter++;
                }
            }
            unsortedArrayLength--;
        }
    }
}
