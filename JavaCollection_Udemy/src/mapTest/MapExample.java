package mapTest;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapExample {
    public static void main(String[] args) {
        Map<String, Double> fruit = new HashMap<>();
        fruit.put("Orange", 1.05);
        fruit.put("Mango", 1.05);
        fruit.put("Apple", 1.05);
        fruit.put("Lemon", 1.39);
        System.out.println(fruit);

        // By using entry set
        for(Map.Entry<String, Double> entry: fruit.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue()); //Apple = 1.05  Mango = 1.05 Orange = 1.05 Lemon = 1.39
        }
        System.out.println("----------");
        // By using keySet
        for(String key: fruit.keySet()) {
            System.out.println(key + " = " + fruit.get(key));  //Apple = 1.05  Mango = 1.05 Orange = 1.05 Lemon = 1.39
        }


    }


}
