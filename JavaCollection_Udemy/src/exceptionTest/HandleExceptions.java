package exceptionTest;

public class HandleExceptions {
    public static void main(String[] args) {
        try {
            int num = Integer.parseInt("10");
            System.out.println(num);

            String str = null;
            System.out.println(str.length());

        } catch (NumberFormatException | NullPointerException e) {
            System.out.println("There was exception" + e);
        }
    }
}
