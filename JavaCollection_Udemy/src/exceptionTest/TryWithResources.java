package exceptionTest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TryWithResources {
    public static void main(String[] args) {
        String path = "/Users/minhvo/IdeaProjects/java_interview_tester/JavaCollection_Udemy/test.txt";
        BufferedReader read = null;
        try {
            read = new BufferedReader(new FileReader(path));
            read.lines().forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (read != null)
                try {
                    read.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
