package stringTest;


public class EqualToWithObjects {
    public static void main(String[] args) {
         Book bookOne = new Book("ICA", 450);
        Book bookTwo = new Book("ICA", 450);

        bookTwo =bookOne; //if you this line, below will be true
        // == check if two references are pointing the object or not
        System.out.println(bookOne == bookTwo); // true if we assign bookTwo = bookOne, if not it will false

    }
}

class Book {
    public String title;
    public  int pages;

    public Book(String title, int pages) {
        this.title = title;
        this.pages = pages;
    }
}
