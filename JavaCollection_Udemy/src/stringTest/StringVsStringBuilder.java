package stringTest;

public class StringVsStringBuilder {
    public static void main(String[] args) {
        String strInput = "12java5python980js";
        StringBuilder output = new StringBuilder();
        for(char ch: strInput.toCharArray()) {
            if(Character.isAlphabetic(ch)) {
                output.append(ch);
            }
        }
        System.out.println(output); //javapythonjs
    }
}
