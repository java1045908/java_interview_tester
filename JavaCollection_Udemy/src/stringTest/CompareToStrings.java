package stringTest;

public class CompareToStrings {
    public static void main(String[] args) {
        String strOne = "apple";
        String strTwo  = new String("apple");
        System.out.println(strOne == strTwo); //false because the string pool
        System.out.println(strOne.equals(strTwo)); //true
    }
}
